import { task } from 'hardhat/config';
import {
  TASK_NODE_SERVER_READY,
  TASK_CLEAN,
  TASK_COMPILE,
} from 'hardhat/builtin-tasks/task-names';
import { TASK_DEPLOY_MULTI } from './deploy-multi';

task(
  TASK_NODE_SERVER_READY,
  'Extended task with ability to auto deploy contract on local server',
).setAction(async (taskArgs, hre, runSuper) => {
  await runSuper(taskArgs);

  if (process.env.DEPLOY_ON_NODE_SERVER_READY === 'true') {
    console.log('Deploying smart contracts to node...');

    await hre.run(TASK_CLEAN);
    await hre.run(TASK_COMPILE);
    await hre.run(TASK_DEPLOY_MULTI);
  }
});
