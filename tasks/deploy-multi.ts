import { TASK_DEPLOY } from './deploy';
import { task, types } from 'hardhat/config';
import path from 'path';
import * as fs from 'fs';
import {
  ContractDeploySchemaType,
  multiContractDeployDto,
} from './schemas/contract-deploy.schema';

export const TASK_DEPLOY_MULTI = 'deploy:multi';

task(TASK_DEPLOY_MULTI, 'Deploy all contracts on blockchain')
  .addOptionalParam(
    'jsonConfigFile',
    'Path to json config file',
    undefined,
    types.string,
  )
  .setAction(async (taskArgs: { jsonConfigFile: string }, hre) => {
    const { jsonConfigFile = 'deploy-multi.config.json' } = taskArgs;

    const resolvedConfigFile = path.resolve(jsonConfigFile);
    const configData: ContractDeploySchemaType[] = JSON.parse(
      fs.readFileSync(resolvedConfigFile, 'utf8'),
    );

    const validationResult = multiContractDeployDto.safeParse(configData);
    if (validationResult.error) throw validationResult.error;

    await Promise.allSettled(
      configData.map((contractData: ContractDeploySchemaType) =>
        hre.run(TASK_DEPLOY, contractData).then((deployedContract) => {
          if (contractData.ethernalWorkspace) {
            return hre.ethernal.push({
              name: contractData.contractName,
              address: deployedContract.target,
              workspace: contractData.ethernalWorkspace,
            });
          }
        }),
      ),
    );
  });
