import { expect } from 'chai';
import hre from 'hardhat';
import { D, D__factory } from '../typechain-types';

describe('Contract D', function () {
  let ContractD: D__factory, contractD: D;

  before(async function () {
    ContractD = await hre.ethers.getContractFactory('D');
  });

  beforeEach(async function () {
    contractD = await ContractD.deploy();
  });

  it('should set variable c to 2 after deployment', async function () {
    expect(await contractD.getC()).to.equal(2);
  });

  it('should allow deposits through C implementation', async function () {
    await contractD.deposit(10);
    expect(await contractD.getDeposited()).to.equal(10);
  });

  it('should initialize deposited to 1 after deployment', async function () {
    expect(await contractD.getDeposited()).to.equal(1);
  });

  it('should revert when calling deposit from B', async function () {
    try {
      await contractD.deposit(0);
    } catch (error: unknown) {
      expect((error as Error).message).to.include('Amount is zero');
    }
  });
});
