import '@nomicfoundation/hardhat-toolbox';
import '@nomiclabs/hardhat-solhint';
import * as dotenv from 'dotenv';
import 'hardhat-ethernal';
import { HardhatUserConfig } from 'hardhat/config';
import './tasks';
dotenv.config();

const config: HardhatUserConfig = {
  solidity: '0.8.20',
  networks: {
    hardhat: {},
    sepolia: {
      accounts: process.env.SEPOLIA_PRIVATE_KEYS?.split(','),
      url: process.env.SEPOLIA_RPC_URL,
    },
  },
  ethernal: {
    uploadAst: true,
    resetOnStart: process.env.ETHERNAL_WORKSPACE,
    workspace: process.env.ETHERNAL_WORKSPACE,
    disabled: process.env.ETHERNAL_DISABLED === 'true',
    disableSync: false,
    disableTrace: false,
  },
  sourcify: {
    enabled: true,
  },
  etherscan: {
    apiKey: {
      sepolia: process.env.ETHERSCAN_API_KEY as string,
    },
  },
};

export default config;
