import { expect } from 'chai';
import hre from 'hardhat';
import { StorageCollider } from '../typechain-types';

describe('Contract StorageCollider', function () {
  let storageCollider: StorageCollider;

  beforeEach(async function () {
    const StorageLibrary =
      await hre.ethers.getContractFactory('StorageLibrary');
    const storageLibrary = await StorageLibrary.deploy();
    const storageLibraryAddress = await storageLibrary.getAddress();

    // Deploy StorageCollider with the library linked
    const StorageColliderFactory = await hre.ethers.getContractFactory(
      'StorageCollider',
      {
        libraries: {
          StorageLibrary: storageLibraryAddress,
        },
      },
    );
    storageCollider = await StorageColliderFactory.deploy();
  });

  it('should initialize with an empty array', async function () {
    const initialArray = await storageCollider.getArray();
    expect(initialArray.length).to.equal(0);
  });

  it('should modify the array with the correct values after collide()', async function () {
    await storageCollider.collide();

    const array = await storageCollider.getArray();
    expect(array.length).to.equal(13);
    expect(array[0]).to.equal(1);
    expect(array[1]).to.equal(2);
    expect(array[12]).to.equal(13);
  });
});
