import { z } from 'zod';

export const contractDeployDto = z
  .object({
    contractName: z.string(),
    libraries: z.record(z.string(), z.string()).optional(),
    contractArgs: z.array(z.union([z.string(), z.number()])).optional(),
    ethernalWorkspace: z.string().optional(),
  })
  .strict();

export const multiContractDeployDto = z.array(contractDeployDto).nonempty();

export type ContractDeploySchemaType = z.infer<typeof contractDeployDto>;
