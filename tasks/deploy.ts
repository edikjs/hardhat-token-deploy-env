import { task, types } from 'hardhat/config';

export const TASK_DEPLOY = 'deploy';

task(TASK_DEPLOY, 'Deploy contract on blockchain')
  .addParam('contractName', 'Smart contract name', undefined, types.string)
  .addOptionalParam(
    'contractArgs',
    'Stringified array of smart contract arguments',
    undefined,
    types.json,
  )
  .addOptionalParam(
    'libraries',
    'Stringified object of external libraries addresses',
    undefined,
    types.json,
  )
  .setAction(
    async (
      taskArgs: {
        contractName: string;
        contractArgs?: unknown[];
        libraries?: Record<string, string>;
      },
      hre,
    ) => {
      const [deployer] = await hre.ethers.getSigners();

      console.log('Deploying contracts with the account:', deployer.address);

      const constractFactory = await hre.ethers.getContractFactory(
        taskArgs.contractName,
        { signer: deployer, libraries: taskArgs.libraries },
      );

      const deployedContract = await constractFactory.deploy(
        ...(taskArgs.contractArgs ?? []),
      );

      const contractAddress = await deployedContract.getAddress();

      console.log('Deployed contract address:', contractAddress);
      console.log(
        'Deployed contract transaction:',
        deployedContract.deploymentTransaction()?.hash,
      );

      return deployedContract;
    },
  );
