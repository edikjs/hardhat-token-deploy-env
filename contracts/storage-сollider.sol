// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.20;

library StorageLibrary {
    function modifyStorageArray(uint256[] memory newArray) external {
        require(newArray.length > 0, "Array is empty");
        uint256[] storage arrayStorage;

        assembly {
            arrayStorage.slot := 0
        }

        uint256 length = arrayStorage.length;

        for (uint256 i = 0; i < length; i++) {
            arrayStorage.pop();
        }

        for (uint256 i = 0; i < newArray.length; i++) {
            arrayStorage.push(newArray[i]);
        }
    }
}

abstract contract ArrayStorage {
    uint256[] private array;

    function collide() external virtual;

    function getArray() external view returns (uint256[] memory) {
        return array;
    }
}

contract StorageCollider is ArrayStorage {
    function collide() external override {
        uint256 length = 13;
        uint256[] memory newArray = new uint256[](length);

        for (uint256 i = 0; i < length; i++) {
            newArray[i] = i + 1;
        }

        StorageLibrary.modifyStorageArray(newArray);
    }
}
