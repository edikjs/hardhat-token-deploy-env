# Smart Contract Development Environment
Hardhat framework example usage.

## Getting Started

### Requirements

- node.js - 18.18.0
- npm - 9.8.1
- solidity - 0.8.20

### Installation

#### Dependencies
```bash
npm install
```

#### Blockchain explorer

Create account and workspace on https://app.tryethernal.com/ (use http://127.0.0.1:8545 as RPC url for localhost) and set corresponding environment variables form `.env.example`. You can get api key from https://app.tryethernal.com/settings?tab=account. Some settings can be customized in `hardhat.config.ts`

#### Real networks

Create account on https://app.infura.io/ and get API key. Add new network in `hardhat.config.ts` with your RPC url and wallet private key.

### Scripts

1. `npm run compile:contract` - compiles contract from .sol file
2. `npm run start:dev` - starts blockchain node server. Compile and deploy contract files to node if `DEPLOY_ON_NODE_SERVER_READY` env variable is true
3. `npm run deploy:contract` - deploys contract to preconfigured network (default network is local node). Example:
```bash
npm run deploy:contract -- --network sepolia --contract-name 'StorageCollider' --libraries '{"StorageLibrary": "0x49EED493555a049C0fd7D48844E73FB1011578dD"}' --contract-args '["1", "2"]'
```
4. `npm run deploy:multi` - deploy contracts by its config (default file name is `deploy-multi.config.json`) to some network. Example:
```bash
npm run deploy:multi -- --network localhost --json-config-file 'deploy-multi.config.json'
```
5. `npm run verify:contract` - verify contract on some network
Example:
```bash
npm run verify:contract -- --network sepolia 0x0000000000000000000000000000000000000000 "PARAM_1" "PARAM_2"
```
